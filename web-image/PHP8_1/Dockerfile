FROM php:8.1-fpm

MAINTAINER "Magento"

ENV PHP_EXTRA_CONFIGURE_ARGS="--enable-fpm --with-fpm-user=magento2 --with-fpm-group=magento2"

RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    sudo \
    wget \
    unzip \
    cron \
    curl \
    libmcrypt-dev \
    libicu-dev \
    libxml2-dev libxslt1-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libzip-dev \
    git \
    vim \
    openssh-server \
    supervisor \
    default-mysql-client \
    ocaml \
    expect \
    htop \
    nano  \
    && apt-get clean

RUN curl -L https://github.com/bcpierce00/unison/releases/download/v2.53.2/unison-v2.53.2+ocaml-4.14.1+x86_64.linux.tar.gz | tar zxv -C /tmp && \
             cd /tmp/bin && \
#             sed -i -e 's/GLIBC_SUPPORT_INOTIFY 0/GLIBC_SUPPORT_INOTIFY 1/' src/fsmonitor/linux/inotify_stubs.c && \
#             make && \
             cp unison unison-fsmonitor /usr/local/bin && \
             cd /root && rm -rf /tmp/bin

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-configure hash --with-mhash \
    && docker-php-ext-install -j$(nproc) intl xsl gd zip pdo_mysql opcache soap bcmath
    # now installed with theimage json iconv

# Install composer (version 2.2)
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN echo composer self-update --2.2
# install xdebug
RUN pecl install xdebug && docker-php-ext-enable xdebug \
    && echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_autostart=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.default_enable=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_host=127.0.0.1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.max_nesting_level=1000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && chmod 666 /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# configure opcache
RUN echo "opcache.memory_consumption=128" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.interned_strings_buffer=8" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.max_accelerated_files=4000" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.revalidate_freq=2" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.fast_shutdown=1" >> /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN docker-php-ext-install sockets \
    && mkdir /var/run/sshd \
    && apt-get install -y --no-install-recommends gnupg && curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash

RUN apt-get install -y --no-install-recommends \
     nodejs \
    npm \
#    && ln -s /usr/bin/nodejs /usr/bin/node \
    && npm install -g grunt-cli

# Install apache2
RUN apt-get install -y --no-install-recommends apache2 \
#    && npm update -g npm && npm install -g grunt-cli && npm install -g gulp \
    && echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config \
    && a2enmod rewrite \
    && a2enmod proxy \
    && a2enmod proxy_fcgi \
    && a2enmod headers \
    && rm -f /etc/apache2/sites-enabled/000-default.conf \
    && useradd -m -d /home/magento2 -s /bin/bash magento2 && adduser magento2 sudo \
    && echo "magento2 ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
    && touch /etc/sudoers.d/privacy \
    && echo "Defaults        lecture = never" >> /etc/sudoers.d/privacy \
    && mkdir /home/magento2/magento2 && mkdir /var/www/magento2 \
    && mkdir /home/magento2/state \
    && curl -sS https://accounts.magento.cloud/cli/installer -o /home/magento2/installer \
    && rm -r /usr/local/etc/php-fpm.d/* \
    && sed -i 's/www-data/magento2/g' /etc/apache2/envvars

# Install tools
RUN apt-get install -y --no-install-recommends \
    #Install Dos2Unix
    tofrodos dos2unix \
    #Install MailCatcher
    rubygems build-essential ruby-dev libsqlite3-dev -y \
    && gem install mailcatcher --document  \
    && apt-get clean

# PHP config
ADD conf/php.ini /usr/local/etc/php

# PHP Libsodium (https://github.com/magento/magento2/issues/23405#issuecomment-506725788)
#RUN echo "deb http://deb.debian.org/debian stretch-backports main" >> /etc/apt/sources.list
#RUN apt-get update && apt-get -t stretch-backports install libsodium-dev -y
RUN curl -O https://download.libsodium.org/libsodium/releases/libsodium-1.0.18.tar.gz \
    && tar xfvz libsodium-1.0.18.tar.gz \
    && cd libsodium-1.0.18 \
    && ./configure \
    && make && make install \
    && pecl install -f libsodium
RUN docker-php-ext-install sodium

# AMQPLIB
RUN apt-get install -y --no-install-recommends \
        librabbitmq-dev \
        libssh-dev \
    && pecl install amqp \
    && docker-php-ext-enable amqp

# Cleanup
RUN apt-get clean

# SSH config
COPY conf/sshd_config /etc/ssh/sshd_config
RUN chown magento2:magento2 /etc/ssh/ssh_config

# supervisord config
ADD conf/supervisord.conf /etc/supervisord.conf

# php-fpm config
ADD conf/php-fpm-magento2.conf /usr/local/etc/php-fpm.d/php-fpm-magento2.conf

# apache config
ADD conf/apache-default.conf /etc/apache2/sites-enabled/apache-default.conf

# unison script
ADD conf/.unison/magento2.prf /home/magento2/.unison/magento2.prf

ADD conf/unison.sh /usr/local/bin/unison.sh
ADD conf/entrypoint.sh /usr/local/bin/entrypoint.sh
ADD conf/check-unison.sh /usr/local/bin/check-unison.sh
RUN dos2unix /usr/local/bin/unison.sh && dos2unix /usr/local/bin/entrypoint.sh \
    && dos2unix /usr/local/bin/check-unison.sh
RUN chmod +x /usr/local/bin/unison.sh && chmod +x /usr/local/bin/entrypoint.sh \
    && chmod +x /usr/local/bin/check-unison.sh

ENV PATH $PATH:/home/magento2/scripts/:/home/magento2/.magento-cloud/bin
ENV PATH $PATH:/var/www/magento2/bin

ENV USE_SHARED_WEBROOT 1
ENV SHARED_CODE_PATH /var/www/magento2
ENV WEBROOT_PATH /var/www/magento2
ENV MAGENTO_ENABLE_SYNC_MARKER 0

RUN mkdir /windows \
 && cd /windows \
 && curl -L -o unison-windows.zip https://github.com/bcpierce00/unison/releases/download/v2.53.2/unison-v2.53.2+ocaml-4.14.0+mingw64c+x86_64.windows.zip \
 && unzip unison-windows.zip \
 && rm unison-windows.zip \
 && mv 'bin/unison.exe' unison.exe \
 && mv 'bin/unison-fsmonitor.exe' unison-fsmonitor.exe \
# && rm 'unison 2.51.2 GTK.exe' \
 && chown -R magento2:magento2 .

#RUN mkdir /mac-osx \
# && cd /mac-osx \
# && curl -L -o unison-mac-osx.zip https://github.com/bcpierce00/unison/releases/download/v2.51.2/Unison-2.51.4.OS.X.zip \
# && unzip unison-mac-osx.zip \
# && rm unison-mac-osx.zip \
# && chown -R magento2:magento2 .

# Initial scripts
COPY scripts/ /home/magento2/scripts/
RUN sed -i 's/^/;/' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && cd /home/magento2/scripts && composer install && chmod +x /home/magento2/scripts/m2init \
    && sed -i 's/^;;*//' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN chown -R magento2:magento2 /home/magento2 && \
    chown -R magento2:magento2 /var/www/magento2 && \
    chmod 755 /home/magento2/scripts/bin/magento-cloud-login

# Delete user password to connect with ssh with empty password
RUN passwd magento2 -d

EXPOSE 80 22 5000 44100
WORKDIR /home/magento2

USER root

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
