@ECHO OFF
REM Read .env settings
for /f "delims== tokens=1,2" %%G in (%~dp0.env) do set %%G=%%H



ECHO Build docker images
docker-compose -f %~dp0docker-compose.yml up --build -d

ECHO Install additional software
REM TODO: move this to own docker image
docker-compose -f %~dp0docker-compose.yml exec web apt-get update
docker-compose -f %~dp0docker-compose.yml exec web apt-get install nano dos2unix htop p7zip p7zip-full

ECHO Initialize source
START %~dp0\scripts\init

ECHO Import database
START %~dp0\scripts\importdb

ECHO Enable Apache Header module
docker-compose -f %~dp0docker-compose.yml exec web a2enmod headers && docker-compose -f %~dp0docker-compose.yml exec web service apache2 restart






