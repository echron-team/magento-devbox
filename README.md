# Magento Devbox #

### Make sure to set the max_map_count high enough for elasticsearch

https://gist.github.com/jarek-przygodzki/4721932bba3bd434512ae6cc58f508b0
https://stackoverflow.com/a/66547784/4575144

**Connect to wsl**

```
wsl -d docker-desktop

wsl -d docker-desktop --exec bash -c "echo 'vm.max_map_count = 262143' > /etc/sysctl.conf"
wsl -d docker-desktop --exec bash -c 'sysctl -p'

wsl -d docker-desktop --exec sysctl -p
```

**Alter configuration**

```
echo "vm.max_map_count = 262144" > /etc/sysctl.conf
```

**Reload configuration**

```
sysctl -p
```

## Installation

### Install blackfire

https://blackfire.io/docs/integrations/chrome

### Disable xdebug

```
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
```

### Install Magento

```
bin/magento setup:install \
--base-url=http://localhost:30208/ \
--db-host=db \
--db-name=magento2 \
--db-user=root \
--db-password=root \
--admin-firstname=Stijn \
--admin-lastname=Duynslaeger \
--admin-email=stijn@echron.com \
--admin-user=admin \
--admin-password=xH6PdVQcM9AuHqvv \
--language=en_GB \
--currency=GBP \
--timezone=Europe/London \
--use-rewrites=1 \
--elasticsearch-host=elasticsearch
```

### Set composer version

```
docker-compose exec web composer self-update --1
```

```
docker-compose exec web composer self-update --2.2
```

### Install Magerun 2

```
wget https://files.magerun.net/n98-magerun2.phar
```

## Available command

### Set up devbox

  ```
  pip install -r .\requirements.txt
  ```

### Start devbox

  ```
  py start.py
  ```

### Connect to web container (as magento2 user)

 ```
py connect.py
 ```

### Connect to web container (as root)

 ```
py connect-root.py
 ```

### Open site in browser

 ```
py visit.py
 ```

### Clear cache

 ```
py flushcache.py
 ```

### Run unittests

 ```
py unittest.py
 ```

### Generate urn files + enable in IDE

 ```
py urn.py
 ```

# Import database

1 Create dump on remote machine

```bash
php n98-magerun2.phar db:dump --add-routines --strip="@stripped" --no-tablespaces --compression gzip dbdump.sql.gz
```

2 Download dump to local machine and place in devbox/_import

3 Run command to import dump into local database (make sure database magento2 is empty or does not exists)

```python
py
importdb.py
```


Magento requirements

https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements.html
