@ECHO OFF
REM Read .env settings
for /f "delims== tokens=1,2" %%G in (.env) do set %%G=%%H
ECHO Build docker images %PROJECT_ID% "PROJECT_ID"

ECHO Enable Apache Header module
docker-compose exec web a2enmod headers && docker-compose exec web service apache2 restart

ECHO Update database

docker-compose exec db mysql -h localhost -u root -proot magento2 -e "UPDATE `core_config_data` SET `value`='http://local.devbox:30%PROJECT_ID%8/' WHERE `path`='web/unsecure/base_url';"
docker-compose exec db mysql -h localhost -u root -proot magento2 -e "UPDATE `core_config_data` SET `value`='http://local.devbox:30%PROJECT_ID%8/' WHERE `path`='web/secure/base_url';"

docker-compose exec db mysql -h localhost -u root -proot magento2 -e "UPDATE `core_config_data` SET `value`='0' WHERE `path`='dev/css/merge_css_files' LIMIT  1;"
docker-compose exec db mysql -h localhost -u root -proot magento2 -e "UPDATE `core_config_data` SET `value`='0' WHERE `path`='dev/css/minify_files' LIMIT  1;"
docker-compose exec db mysql -h localhost -u root -proot magento2 -e "UPDATE `core_config_data` SET `value`='0' WHERE `path`='dev/static/sign' LIMIT  1;"


ECHO Clear cache
docker-compose exec --user=magento2 web php /var/www/magento2/bin/magento cache:clean


ECHO Project is available at http://local.devbox:30%PROJECT_ID%8/