#!/usr/bin/env python
import os

from lib.devbox import Devbox
from lib.utils import output

devbox = Devbox()

# ---
# Fix for elasticsearch max_map_count to low on VM
# ---

# Current max_map_count
os.system('wsl -d docker-desktop cat /proc/sys/vm/max_map_count')
# Set current max_map_count to 262144
os.system("wsl -d docker-desktop sysctl -w vm.max_map_count=262144")

print('Starting devbox')
# TODO: only start when not already running
running = devbox.run()
if not running:
    output('Devbox not started', 'info')
else:
    # TODO: verify if containers are running

    print('Start sync')
    # TODO: check if sync is not already running
    devbox.start_sync()

    # ECHO Start sync
    # echo. 2>%~dp0/shared/state/enable_sync
    # START %~dp0\scripts\unison-sync.bat

    # TODO: once sync is complete run 'npm install' and maybe composer install
    # REM TODO: wait till docker is started up
    # TIMEOUT /T 10

    print('Frontend: ' + "\t" + devbox.get_frontend_url())
    print('Mailcatcher: ' + "\t" + devbox.get_mailcatcher_url())
    print('RabbitMQ: ' + "\t" + devbox.get_rabbitmq_url())
    print('PHPMyAdmin: ' + "\t" + devbox.get_phpmyadmin_url())
    # ECHO Frontend: http://localhost:30%PROJECT_ID%8
    # ECHO Mailcatcher: http://localhost:30%PROJECT_ID%7
    # ECHO RabbitMQ: http://localhost:30%PROJECT_ID%9
    # start "" http://localhost:30%PROJECT_ID%8

    # REM Connect to web container
    # docker-compose -f %~dp0docker-compose.yml exec --user=magento2 web /bin/bash -c "cd /var/www/magento2; exec '/bin/bash'"

    devbox.docker.connect('web', 'magento2')
