@ECHO OFF


REM Read .env settings
for /f "delims== tokens=1,2" %%G in (%~dp0.env) do set %%G=%%H

docker-compose -f %~dp0docker-compose.yml exec --user=magento2 --workdir=/var/www/magento2 web php %*
