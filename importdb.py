#!/usr/bin/env python
# python .\flushcache.py
# (windows shell) flushcache.py
from lib.devbox import Devbox

devbox = Devbox()
# devbox.database_sql('SELECT * FROM core_config_data')

# List locations where backup can be + make list of all possible backup files (.sql.gz)
# Check if database currently exists, if so, make sure it's completely removed!
# Make sure we have magerun installed (wget https://files.magerun.net/n98-magerun2.phar)
# Import selected file php n98-magerun2.phar db:import --compression=gz databasedump.sql.gz

databaseDumpFile = '/var/tmp/import/database.sql.gz'

# devbox.magerun2('db:import --compression=gz ' + databaseDumpFile)

# Update configuration

devbox.core_config_set('web/unsecure/base_url', '' + devbox.get_frontend_url() + '/')
devbox.core_config_set('web/secure/base_url', '' + devbox.get_frontend_url() + '/')
devbox.core_config_set('admin/url/custom', '' + devbox.get_frontend_url() + '/')

# disable use of secured urls
devbox.core_config_set('web/secure/enable_hsts', '0')
devbox.core_config_set('web/secure/enable_upgrade_insecure', '0')

# disable css mergin, minifying and signing


devbox.core_config_set('dev/css/merge_css_files', '0')
devbox.core_config_set('dev/css/minify_files', '0')
devbox.core_config_set('dev/static/sign', '0')
devbox.core_config_set('dev/css/use_css_critical_path', '1')
devbox.core_config_set('dev/js/move_script_to_bottom', '1')
devbox.core_config_set('dev/template/minify_html', '0')
devbox.core_config_set('dev/js/enable_js_bundling', '0')
devbox.core_config_set('dev/quickdevbar/enable', '1')

# in production
devbox.core_config_set('dev/css/minify_files', '1')
devbox.core_config_set('dev/static/sign', '1')
devbox.core_config_set('dev/template/minify_html', '1')

# set GD2 as default image adapter
devbox.core_config_set('dev/image/default_adapter', 'GD2')

# Elasticsearch
devbox.core_config_set('catalog/search/elasticsearch7_server_hostname', 'https://elasticsearch')
devbox.core_config_set('catalog/search/elasticsearch7_server_port', '9200')
devbox.core_config_set('catalog/search/elasticsearch7_index_prefix', 'magento2')
devbox.core_config_set('catalog/search/elasticsearch7_enable_auth', '0')
devbox.core_config_set('catalog/search/elasticsearch7_server_timeout', '15')
devbox.core_config_set('catalog/search/engine', 'elasticsearch8')

# enable redis cache
# devbox.bin_magento('setup:config:set setup:config:set --no-interaction --cache-backend=redis --cache-backend-redis-server=redis --cache-backend-redis-db=0')

# enable redis session storage
# devbox.bin_magento('setup:config:set --no-interaction --session-save=redis --session-save-redis-host=redis --session-save-redis-log-level=3 --session-save-redis-db=2')

# Clear cache
devbox.bin_magento('cache:flush')

devbox.bin_magento('deploy:mode:set developer')
