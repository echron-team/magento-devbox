@ECHO OFF
REM Read .env settings

for /f "delims== tokens=1,2" %%G in (%~dp0\..\.env) do set %%G=%%H



ECHO Cache flush
docker exec -it magento2devbox_redis_%COMPOSE_PROJECT_NAME% redis-cli flushall



