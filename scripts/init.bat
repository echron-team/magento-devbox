@ECHO OFF
REM Read .env settings

for /f "delims== tokens=1,2" %%G in (%~dp0\..\.env) do set %%G=%%H

ECHO Initialize source (make sure the docker containers are running)

if not exist %TEMP%\%COMPOSE_PROJECT_NAME% mkdir %TEMP%\%COMPOSE_PROJECT_NAME%


SET ARCH=%TEMP%\%COMPOSE_PROJECT_NAME%\magento27z.7zip

if exist ARCH (
    ECHO Archive does already exist (remove %ARCH% to force regeneration)
) else (
    ECHO Create/update archive (%ARCH%)
    tools\7zip\App\7-Zip64\7z.exe a -t7z -mx=9 -mmt=30 -bt %ARCH% ..\src\*
)


ECHO Copy archive to docker image
docker cp %ARCH% magento2devbox_web_%COMPOSE_PROJECT_NAME%:/var/www/magento2/magento27z.7zip

ECHO Unpack archive
docker exec -it magento2devbox_web_%COMPOSE_PROJECT_NAME% 7z x -y /var/www/magento2/magento27z.7zip -o/var/www/magento2/
rem docker exec -it magento2devbox_web_%COMPOSE_PROJECT_NAME% rm /var/www/magento27z.7zip
DEL %ARCH%
ECHO Set file rights
docker exec -it magento2devbox_web_%COMPOSE_PROJECT_NAME% chown -R magento2:magento2 /var/www/magento2

ECHO Install packages
docker exec -it magento2devbox_web_%COMPOSE_PROJECT_NAME% --user=magento2 composer --working-dir=/var/www/magento2 install



FOR /f "delims=" %%A IN ('docker-compose port web 80') DO SET "CMD_OUTPUT=%%A"
FOR /f "tokens=1,* delims=:" %%A IN ("%CMD_OUTPUT%") DO SET "WEB_PORT=%%B"

ECHO Start filesync
echo. 2>./shared/state/enable_sync
START %~dp0\..\scripts\unison-sync.bat
REM docker-compose exec --user magento2 web m2init magento:reset --no-interaction --webserver-home-port=%WEB_PORT%
PAUSE




