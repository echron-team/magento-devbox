# RUN all: ./vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist


# Single class
# ./vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist app/code/Baldwin/LoginRegister/Test/Check3CXOrderPlacementTest.php


# Integration tests
# ./vendor/bin/phpunit -c dev/tests/integration/phpunit.xml.dist app/code/Baldwin/LoginRegister/Test/Check3CXOrderPlacementTest.php
