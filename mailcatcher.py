#!/usr/bin/env python
from lib.devbox import Devbox
import webbrowser

devbox = Devbox()

webbrowser.open(devbox.get_mailcatcher_url())
