import os
import subprocess


def replace_in_file(file, search, replace):
    f = open(file, 'r')
    filedata = f.read()
    f.close()

    newdata = filedata.replace(search, replace)

    f = open(file, 'w')
    f.write(newdata)
    f.close()


def run_command(command, pipe: bool = True):
    command = command + ""
    # out_debug('[DEVBOX EXEC] ' + command)
    if pipe:
        process = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
        return process.stdout.decode(errors='replace')
    else:
        os.system(command)


def out_debug(text):
    output(text, 'debug')


def output(text, level='debug'):
    if level == 'debug':
        print('\033[35m' + text + '\033[0m')
    elif level == 'info':
        print('\033[34m' + text + '\033[0m')
    elif level == 'error':
        print('\033[31m' + text + '\033[0m')
    else:
        print(text)


def port_used(port: int) -> bool:
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0
