import os

from dotenv import load_dotenv
from lib.docker import Docker
from lib.utils import port_used, output


class Devbox(object):
    condition = 'New'

    def __init__(self):
        load_dotenv()

        self.projectId = os.environ.get('PROJECT_ID')
        self.hostname = os.environ.get('CUSTOM_HOSTNAME')
        if not self.hostname:
            self.hostname = 'localhost'

        self.composerProjectName = os.environ.get('COMPOSE_PROJECT_NAME')
        self.docker = Docker('docker-compose.yml')

    # Run bin/magento command
    def bin_magento(self, command):
        docker_command = f'php /var/www/magento2/bin/magento {command}'
        output = self.docker.docker_exec('web', docker_command, 'magento2')
        return output

    # Set config in database
    def core_config_set(self, path, value):

        # https://github.com/netz98/n98-magerun2?tab=readme-ov-file#get-store-config
        command = f'config:store:set {path} {value} --scope=default --scope-id=0'
        self.magerun2(command)

        # tablePrefix = ''
        # query = f'INSERT INTO `{tablePrefix}core_config_data` (`scope`,`scope_id`,`path`,`value`) VALUES(\'default\',0, \'{path}\', \'{value}\') ON DUPLICATE KEY UPDATE `value`=\'{value}\';'
        # output = self.database_sql(query)
        # print(output)

        query2 = f'DELETE FROM `core_config_data` WHERE `scope` = \'stores\' AND `scope_id` = 0 AND `path` = \'{path}\';'
        output2 = self.database_sql(query2)
        print(output2)

    def magerun2(self, command):
        # TODO: make sure magerun2 is installed (wget https://files.magerun.net/n98-magerun2.phar)
        docker_command = f'php /var/www/magento2/n98-magerun2.phar --root-dir /var/www/magento2 {command}'
        print(docker_command)
        output = self.docker.docker_exec('web', docker_command, 'magento2')
        print(output)

    def start_sync(self):
        os.system(f'START "{self.composerProjectName}" scripts\\unison-sync.bat')

    def get_frontend_url(self):
        return f'http://{self.hostname}:30{self.projectId}8'

    def get_backend_url(self):
        adminUri = self.bin_magento('info:adminuri')
        adminUri = adminUri.replace("\n", '')
        adminUri = adminUri.replace('Admin URI: ', '')
        return f'http://{self.hostname}:30{self.projectId}8{adminUri}'

    def get_mailcatcher_url(self):
        return f'http://{self.hostname}:30{self.projectId}7'

    def get_rabbitmq_url(self):
        return f'http://{self.hostname}:30{self.projectId}9'

    def get_phpmyadmin_url(self):
        return f'http://{self.hostname}:3{self.projectId}10'

    def run(self) -> bool:
        portWeb = int(f'30{self.projectId}8')
        portWebUsed = port_used(portWeb)
        if portWebUsed:
            output(f'Port "{portWeb}" for web already in use', 'error')
            return False

        self.docker.up()
        # TODO: validate that docker images are running
        return True

    def halt(self) -> bool:
        self.docker.stop()
        return True

    def flush_cache(self):
        self.docker.docker_exec('redis', 'redis-cli flushall')

    def database_sql(self, query):
        output = self.docker.docker_exec('db', f'mysql -h localhost -u root -proot magento2 -e "{query}"')
        print(output)

    def grunt_watch(self):
        # grunt refresh && grunt exec:verlichting_nl && grunt less:verlichting_nl && grunt watch
        self.docker.docker_exec('web',
                                '/bin/sh -c "grunt clean && grunt exec && grunt less:verlichting_nl && grunt watch"',
                                'magento2',
                                '/var/www/magento2', False)

    # output = self.docker.docker_exec('web', 'grunt exec', 'magento2', '/var/www/magento2')
    # print(output)
    # output = self.docker.docker_exec('web', 'grunt less', 'magento2', '/var/www/magento2')
    # print(output)
    # output = self.docker.docker_exec('web', 'grunt watch', 'magento2', '/var/www/magento2')
    # print(output)

    def log_watch(self):
        output = self.docker.docker_exec('web', 'tail -n 1 -f /var/www/magento2/var/log/system.log', 'magento2',
                                         '/var/www/magento2')
        print(output)

    def status(self):
        services = ['web', 'db', 'elasticsearch', 'redis']
        for service in services:
            is_running = self.docker.is_running(service)
            if not is_running:
                output(f'{service}: not running', 'error')
            else:
                output(f'{service}: running')
