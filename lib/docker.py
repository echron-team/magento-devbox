from dotenv import load_dotenv
from lib.utils import run_command, output


class Docker(object):
    condition = 'New'

    def __init__(self, docker_compose_file):
        load_dotenv()
        self.docker_compose_file = docker_compose_file

    def docker_compose(self, command, pipe: bool = True):
        bash_command = f"docker-compose -f {self.docker_compose_file} {command}"

        print(bash_command)
        response = run_command(bash_command, pipe)
        return response

    # 'exec' a command on a container
    def docker_exec(self, container, command, as_user='', workdir='', pipe: bool = True):
        docker_command = f"exec"

        if len(as_user):
            docker_command = docker_command + f" --user={as_user}"

        if len(workdir):
            docker_command = docker_command + f" --workdir={workdir}"

        docker_command = docker_command + f" {container} {command}"

        response = self.docker_compose(docker_command, pipe)
        return response

    # Get file from container to host
    def get_file(self, container, container_source, host_destination):
        docker_command = f'cp {container}:{container_source}  {host_destination}'
        response = self.docker_compose(docker_command)
        output(response)

    # connect to container
    def connect(self, container='web', as_user=''):
        extra = ''
        if len(as_user):
            extra = f" --user={as_user}"

        if container == 'web':
            extra = extra + ' --workdir=/var/www/magento2'

        docker_command = f'exec {extra} {container} /bin/bash'

        self.docker_compose(docker_command, False)
        # os.system(f'docker-compose -f {self.docker_compose_file} {docker_command}')

    def up(self):
        self.docker_compose('up -d')

    def stop(self):
        self.docker_compose('stop')

    def is_running(self, container) -> bool:
        docker_command = f'exec {container} echo "hello world"'
        response = self.docker_compose(docker_command)
        response = response.strip()
        if response == 'hello world':
            return True
        else:
            return False
