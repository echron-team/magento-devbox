#!/usr/bin/env python
from lib.devbox import Devbox

devbox = Devbox()

devbox.status()

print('Frontend: ' + "\t" + devbox.get_frontend_url())
print('Backend: ' + "\t" + devbox.get_backend_url())
print('Mailcatcher: ' + "\t" + devbox.get_mailcatcher_url())
print('RabbitMQ: ' + "\t" + devbox.get_rabbitmq_url())
print('PHPMyAdmin: ' + "\t" + devbox.get_phpmyadmin_url())

# ECHO Frontend: http://localhost:30%PROJECT_ID%8
# ECHO Mailcatcher: http://localhost:30%PROJECT_ID%7
# ECHO RabbitMQ: http://localhost:30%PROJECT_ID%9
# start "" http://localhost:30%PROJECT_ID%8

# REM Connect to web container
# docker-compose -f %~dp0docker-compose.yml exec --user=magento2 web /bin/bash -c "cd /var/www/magento2; exec '/bin/bash'"

# devbox.docker.connect('web', 'magento2')
