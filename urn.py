#!/usr/bin/env python
from lib.devbox import Devbox
from lib.utils import replace_in_file

devbox = Devbox()

# phpstorm|vscode
ide = 'phpstorm'
magentoMiscPath = '/var/www/magento2/misc.xml'
idePath = '../.idea/misc.xml'

x = devbox.docker.docker_exec('web', f'touch {magentoMiscPath}', 'magento2')
print(x)
# TODO: for some reason this seems to throw an error in Magento, but when executed directly in the shell this works
y = devbox.bin_magento(f'dev:urn-catalog:generate --ide={ide} {magentoMiscPath} --verbose')
print(y)

# TODO: we should actually merge the existing misc.xml with this one (we could put the existing file in the container, generate it and then move it back)
devbox.docker.get_file('web', magentoMiscPath, idePath)

# Make sure the files point to the source files in on our host
replace_in_file(idePath, '$PROJECT_DIR$/www/magento2/vendor/', '$PROJECT_DIR$/src/vendor/')
replace_in_file(idePath, '$PROJECT_DIR$/vendor/', '$PROJECT_DIR$/src/vendor/')
